package com.folcademy.clinica.Exceptions;

public class EmptyException extends RuntimeException{
    public EmptyException(String message) {
        super(message);
    }
}
