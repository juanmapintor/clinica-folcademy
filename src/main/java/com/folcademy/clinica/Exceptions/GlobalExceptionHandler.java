package com.folcademy.clinica.Exceptions;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultErrorHandler(HttpServletRequest request, Exception exception){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error general", exception.getMessage(), "1", request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> NotFoundErrorHandler(HttpServletRequest request, Exception exception){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("No se encontró la entidad.", exception.getMessage(), "2", request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmptyException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> EmptyErrorHandler(HttpServletRequest request, Exception exception){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("No hay datos para mostrar.", exception.getMessage(), "3", request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> DataIntegrityViolationErrorHandler(HttpServletRequest request, DataIntegrityViolationException exception){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Los datos no son integros.", exception.getMostSpecificCause().getMessage(), "4", request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ErrorMessage> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(new ErrorMessage("Error de validación", error.getDefaultMessage(), "4", ((ServletWebRequest)request).getRequest().getRequestURI()));
        }
        return new ResponseEntity(details, HttpStatus.BAD_REQUEST);
    }


}
