package com.folcademy.clinica.Exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
@Getter
public class ErrorMessage {
    private String message;
    private String detail;
    private String code;
    private String path;
}
