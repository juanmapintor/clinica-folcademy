package com.folcademy.clinica.Model.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDTOMostrar {
    Integer id;

    @NotEmpty(message = "El campo profesion debe tener un valor")
    String profesion;

    @Positive(message = "La consulta debe ser un numero positivo")
    int consulta;

    @NotNull(message = "El campo idpersona debe tener un valor")
    Integer idpersona;

    PersonaDTOMostrar persona;
}
