package com.folcademy.clinica.Model.DTOs;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDTOCompleto {
    Integer id;
    @NotEmpty(message = "El campo nombre no puede estar vacio")
    String nombre;
    @NotEmpty(message = "El campo apellido no puede estar vacio")
    String apellido;
    @NotEmpty(message = "El campo dni no puede estar vacio")
    String dni;
    @NotEmpty(message = "El campo telefono no puede estar vacio")
    String telefono;
}
