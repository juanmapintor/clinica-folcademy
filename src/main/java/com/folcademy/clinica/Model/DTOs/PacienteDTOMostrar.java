package com.folcademy.clinica.Model.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDTOMostrar {
    Integer id;

    @NotEmpty(message = "El campo direccion debe tener un valor")
    String direccion;

    @NotNull(message = "El campo idpersona debe tener un valor")
    Integer idpersona;

    PersonaDTOMostrar persona;
}
