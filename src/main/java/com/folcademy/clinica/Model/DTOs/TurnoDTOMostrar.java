package com.folcademy.clinica.Model.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDTOMostrar {
    public Integer idturno;

    @NotNull(message = "El campo fecha no puede ser vacio")
    public LocalDate fecha;

    @NotNull(message = "El campo hora no puede ser vacio")
    public LocalTime hora;

    public Boolean atendido;

    @NotNull(message = "El campo idpaciente no puede ser vacio")
    public Integer idpaciente;

    @NotNull(message = "El campo idmedico no puede ser vacio")
    public Integer idmedico;

    public PacienteDTOMostrar paciente;

    public MedicoDTOMostrar medico;
}
