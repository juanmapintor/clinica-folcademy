package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTOs.PacienteDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PacienteDTOMostrar;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    private final PersonaMapper personaMapper;

    public PacienteMapper(PersonaMapper personaMapper) {
        this.personaMapper = personaMapper;
    }

    public PacienteDTOCompleto entityToDTO(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDTOCompleto(
                                ent.getId(),
                                ent.getDireccion(),
                                ent.getIdpersona(),
                                personaMapper.entityToDTO(
                                        ent.getPersona()
                                )
                        )
                )
                .orElse(new PacienteDTOCompleto());
    }

    public Paciente DTOToEntity(PacienteDTOCompleto dto){
        Paciente pacienteEntity = new Paciente();

        pacienteEntity.setId(dto.getId());
        pacienteEntity.setDireccion(dto.getDireccion());
        pacienteEntity.setIdpersona(dto.getIdpersona());
        pacienteEntity.setPersona(
                personaMapper.DTOToEntity(dto.getPersona())
        );

        return pacienteEntity;
    }
    public PacienteDTOMostrar entityToDTOMostrar(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDTOMostrar(
                                ent.getId(),
                                ent.getDireccion(),
                                ent.getIdpersona(),
                                personaMapper.entityToDTOMostrar(
                                        ent.getPersona()
                                )
                        )
                )
                .orElse(new PacienteDTOMostrar());
    }

    public Paciente DTOMostrarToEntity(PacienteDTOMostrar dto){
        Paciente pacienteEntity = new Paciente();

        pacienteEntity.setId(dto.getId());
        pacienteEntity.setDireccion(dto.getDireccion());
        pacienteEntity.setIdpersona(dto.getIdpersona());
        pacienteEntity.setPersona(
                personaMapper.DTOMostrarToEntity(dto.getPersona())
        );

        return pacienteEntity;
    }

}
