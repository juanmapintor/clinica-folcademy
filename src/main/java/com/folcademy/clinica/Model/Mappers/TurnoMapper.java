package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTOs.TurnoDTOCompleto;
import com.folcademy.clinica.Model.DTOs.TurnoDTOEditarAgregar;
import com.folcademy.clinica.Model.DTOs.TurnoDTOMostrar;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    private final PacienteMapper pacienteMapper;
    private final MedicoMapper medicoMapper;

    public TurnoMapper(PacienteMapper pacienteMapper, MedicoMapper medicoMapper) {
        this.pacienteMapper = pacienteMapper;
        this.medicoMapper = medicoMapper;
    }

    public Turno DTOToEntity(TurnoDTOCompleto dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        entity.setPaciente(pacienteMapper.DTOToEntity(dto.getPaciente()));
        entity.setMedico(medicoMapper.DTOToEntity(dto.getMedico()));
        return entity;
    }

    public Turno DTOMostrarToEntity(TurnoDTOMostrar dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        entity.setPaciente(pacienteMapper.DTOMostrarToEntity(dto.getPaciente()));
        entity.setMedico(medicoMapper.DTOMostrarToEntity(dto.getMedico()));
        return entity;
    }

    public Turno DTOEditarAgregarToEntity(TurnoDTOEditarAgregar dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }

    public TurnoDTOCompleto EntityToDTO(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDTOCompleto(
                              ent.getId(),
                              ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico(),
                                pacienteMapper.entityToDTO(ent.getPaciente()),
                                medicoMapper.entityToDTO(ent.getMedico())
                        )
                ).orElse(new TurnoDTOCompleto());
    }
    public TurnoDTOMostrar entityToDTOMostrar(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDTOMostrar(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico(),
                                pacienteMapper.entityToDTOMostrar(ent.getPaciente()),
                                medicoMapper.entityToDTOMostrar(ent.getMedico())
                        )
                ).orElse(new TurnoDTOMostrar());
    }
    public TurnoDTOEditarAgregar EntityToDTOEditarAgregar(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDTOEditarAgregar(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                ).orElse(new TurnoDTOEditarAgregar());
    }
}
