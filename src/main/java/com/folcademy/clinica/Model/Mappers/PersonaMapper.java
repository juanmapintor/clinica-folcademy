package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTOs.PersonaDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PersonaDTOMostrar;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PersonaMapper {

    public PersonaDTOCompleto entityToDTO(Persona entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PersonaDTOCompleto(
                                    ent.getId(),
                                    ent.getNombre(),
                                    ent.getApellido(),
                                    ent.getDni(),
                                    ent.getTelefono()
                                )
                )
                .orElse(new PersonaDTOCompleto());
    }

    public PersonaDTOMostrar entityToDTOMostrar(Persona entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PersonaDTOMostrar(
                                ent.getId(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getDni()
                        )
                )
                .orElse(new PersonaDTOMostrar());
    }

    public Persona DTOToEntity(PersonaDTOCompleto dto){
        Persona entity = new Persona();
        entity.setId(dto.getId());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        entity.setNombre(dto.getNombre());
        entity.setDni(dto.getDni());
        return entity;
    }

    public Persona DTOMostrarToEntity(PersonaDTOMostrar dto){
        Persona entity = new Persona();
        entity.setId(dto.getId());
        entity.setApellido(dto.getApellido());
        entity.setNombre(dto.getNombre());
        entity.setDni(dto.getDni());
        return entity;
    }
}
