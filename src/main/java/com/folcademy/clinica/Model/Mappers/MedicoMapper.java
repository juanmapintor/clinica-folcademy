package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTOs.MedicoDTOCompleto;
import com.folcademy.clinica.Model.DTOs.MedicoDTOMostrar;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    private final PersonaMapper personaMapper;

    public MedicoMapper(PersonaMapper personaMapper) {
        this.personaMapper = personaMapper;
    }

    public MedicoDTOCompleto entityToDTO(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDTOCompleto(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersona(),
                                personaMapper.entityToDTO(
                                        ent.getPersona()
                                )
                        )
                )
                .orElse(new MedicoDTOCompleto());
    }

    public Medico DTOToEntity(MedicoDTOCompleto dto){
        Medico medicoEntity = new Medico();

        medicoEntity.setId(dto.getId());
        medicoEntity.setProfesion(dto.getProfesion());
        medicoEntity.setConsulta(dto.getConsulta());
        medicoEntity.setIdpersona(dto.getIdpersona());
        medicoEntity.setPersona(
                personaMapper.DTOToEntity(
                        dto.getPersona()
                )
        );

        return medicoEntity;
    }

    public MedicoDTOMostrar entityToDTOMostrar(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDTOMostrar(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersona(),
                                personaMapper.entityToDTOMostrar(
                                        ent.getPersona()
                                )
                        )
                )
                .orElse(new MedicoDTOMostrar());
    }


    public Medico DTOMostrarToEntity(MedicoDTOMostrar dto){
        Medico entity = new Medico();

        entity.setId(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(dto.getIdpersona());
        entity.setPersona(
                personaMapper.DTOMostrarToEntity(
                        dto.getPersona()
                )
        );

        return entity;
    }

}
