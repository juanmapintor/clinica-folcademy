package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTOs.PersonaDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PersonaDTOMostrar;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/personas")
public class PersonaController {
    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }

    //@PreAuthorize("hasAuthority('personas_listarTodos')")
    @GetMapping("")
    public ResponseEntity<List<PersonaDTOMostrar>> listarTodos() {
        return ResponseEntity.ok(
                personaService.listarTodos()
        );
    }

    //@PreAuthorize("hasAuthority('personas_listarTodosPorPagina')")
    @GetMapping("/page")
    public ResponseEntity<Page<PersonaDTOMostrar>> listarTodosPorPagina(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "apellido") String orderField

    ) {
        return ResponseEntity.ok(
                personaService.listarTodosPorPagina(pageNumber, pageSize, orderField)
        );
    }

    //@PreAuthorize("hasAuthority('personas_listarUno')")
    @GetMapping("/{idPersona}")
    public ResponseEntity<PersonaDTOMostrar> listarUno(@PathVariable(name = "idPersona") int idPersona){
        return  ResponseEntity.ok(
                personaService.listarUno(idPersona)
        );
    }

    //@PreAuthorize("hasAuthority('personas_listarUnoPorPagina')")
    @GetMapping("/page/{idPersona}")
    public ResponseEntity<Page<PersonaDTOMostrar>> listarUnoPorPagina(@PathVariable(name = "idPersona") int idPersona){
        return  ResponseEntity.ok(
                personaService.listarUnoPorPagina(idPersona)
        );
    }

    //@PreAuthorize("hasAuthority('personas_agregar')")
    @PostMapping("")
    public ResponseEntity<PersonaDTOCompleto> agregar(@Validated @RequestBody PersonaDTOCompleto dto){
        return ResponseEntity.ok(
                personaService.agregar(dto)
        );
    }

    //@PreAuthorize("hasAuthority('personas_borrar')")
    @DeleteMapping("/{idPersona}")
    public ResponseEntity borrar(@PathVariable(name = "idPersona") int idPersona){
        personaService.borrar(idPersona);
        return ResponseEntity.ok().build();
    }

    //@PreAuthorize("hasAuthority('personas_editar')")
    @PutMapping("/{idPersona}")
    public ResponseEntity<PersonaDTOCompleto> editar(@PathVariable(name = "idPersona") int id, @Validated @RequestBody PersonaDTOCompleto dto){
        return ResponseEntity.ok(
                personaService.editar(id, dto)
        );
    }
}
