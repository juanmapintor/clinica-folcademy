package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTOs.TurnoDTOEditarAgregar;
import com.folcademy.clinica.Model.DTOs.TurnoDTOMostrar;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnoController {
    private final TurnoService turnoService;


    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //@PreAuthorize("hasAuthority('turnos_listarTodos')")
    @GetMapping("")
    public ResponseEntity<List<TurnoDTOMostrar>> listarTodos() {
        return ResponseEntity.ok(
                turnoService.listarTodos()
        );
    }

    //@PreAuthorize("hasAuthority('turnos_listarTodosPorPagina')")
    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDTOMostrar>> listarTodosPorPagina(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "fecha") String orderField

    ) {
        return ResponseEntity.ok(
                turnoService.listarTodosPorPagina(pageNumber, pageSize, orderField)
        );
    }

    //@PreAuthorize("hasAuthority('turnos_listarUno')")
    @GetMapping("/{idTurno}")
    public ResponseEntity<TurnoDTOMostrar> listarUno(@PathVariable(name = "idTurno") int idTurno){
        return  ResponseEntity.ok(
                turnoService.listarUno(idTurno)
        );
    }

    //@PreAuthorize("hasAuthority('turnos_listarUnoPorPagina')")
    @GetMapping("/page/{idTurno}")
    public ResponseEntity<Page<TurnoDTOMostrar>> listarUnoPorPagina(@PathVariable(name = "idTurno") int idTurno){
        return  ResponseEntity.ok(
                turnoService.listarUnoPorPagina(idTurno)
        );
    }
    //@PreAuthorize("hasAuthority('turnos_agregar')")
    @PostMapping("")
    public ResponseEntity<TurnoDTOEditarAgregar> agregar(@Validated @RequestBody TurnoDTOEditarAgregar dto){
        return ResponseEntity.ok(
                turnoService.agregar(dto)
        );
    }

    //@PreAuthorize("hasAuthority('turnos_borrar')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity borrar(@PathVariable(name = "idTurno") int idTurno){
        turnoService.borrar(idTurno);
        return ResponseEntity.ok().build();
    }

    //@PreAuthorize("hasAuthority('turnos_editar')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDTOEditarAgregar> editar(@PathVariable(name = "idTurno") int id, @Validated @RequestBody TurnoDTOEditarAgregar dto){
        return ResponseEntity.ok(
                turnoService.editar(id, dto)
        );
    }
}
