package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTOs.PacienteDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PacienteDTOMostrar;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    //@PreAuthorize("hasAuthority('pacientes_findAll')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDTOMostrar>> findAll() {
        return ResponseEntity
                .ok(
                        pacienteService.findAllPacientes()
                );
    }

    //@PreAuthorize("hasAuthority('pacientes_findAllByPage')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDTOMostrar>> listarTodosPorPagina(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "persona.apellido") String orderField

    ) {
        return ResponseEntity.ok(
                pacienteService.findAllByPage(pageNumber, pageSize, orderField)
        );
    }

    //@PreAuthorize("hasAuthority('pacientes_findById')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<PacienteDTOMostrar> findById(@PathVariable(name = "id") int id) {
        return ResponseEntity
                .ok(
                        pacienteService.findPacienteById(id)
                );
    }

    //@PreAuthorize("hasAuthority('pacientes_findByIdByPage')")
    @GetMapping(value = "/page/{id}")
    public ResponseEntity<Page<PacienteDTOMostrar>> findByIdByPage(@PathVariable(name = "id") int id) {
        return ResponseEntity
                .ok(
                        pacienteService.findPacienteByIdByPage(id)
                );
    }

    //@PreAuthorize("hasAuthority('pacientes_save')")
    @PostMapping("")
    public ResponseEntity<PacienteDTOCompleto> save(@Valid @RequestBody PacienteDTOCompleto dto){
        return ResponseEntity.ok(
                pacienteService.savePaciente(dto)
        );
    }

    //@PreAuthorize("hasAuthority('pacientes_deleteById')")
    @DeleteMapping("/{idPaciente}")
    public void deleteById(@PathVariable(name = "idPaciente") int idPaciente){
        pacienteService.deleteById(idPaciente);
    }

    //@PreAuthorize("hasAuthority('pacientes_edit')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDTOCompleto> edit(@PathVariable(name = "idPaciente") int idPaciente, @Valid @RequestBody PacienteDTOCompleto dto){
        return ResponseEntity.ok(
                pacienteService.edit(idPaciente, dto)
        );
    }

}
