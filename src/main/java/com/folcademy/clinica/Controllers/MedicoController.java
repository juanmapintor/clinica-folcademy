package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTOs.MedicoDTOCompleto;
import com.folcademy.clinica.Model.DTOs.MedicoDTOMostrar;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //@PreAuthorize("hasAuthority('medicos_listarTodos')")
    @GetMapping("")
    public ResponseEntity<List<MedicoDTOMostrar>> listarTodos() {
        return ResponseEntity.ok(
                medicoService.listarTodos()
        );
    }

    //@PreAuthorize("hasAuthority('medicos_listarTodosPorPagina')")
    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDTOMostrar>> listarTodosPorPagina(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "persona.apellido") String orderField

    ) {
        return ResponseEntity.ok(
                medicoService.listarTodosPorPagina(pageNumber, pageSize, orderField)
        );
    }
    //@PreAuthorize("hasAuthority('medicos_listarUno')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDTOMostrar> listarUno(@PathVariable(name = "idMedico") int idMedico){
        return  ResponseEntity.ok(
                medicoService.listarUno(idMedico)
        );
    }

    //@PreAuthorize("hasAuthority('medicos_listarUnoPorPagina')")
    @GetMapping("/page/{idMedico}")
    public ResponseEntity<Page<MedicoDTOMostrar>> listarUnoPorPagina(@PathVariable(name = "idMedico") int idMedico){
        return  ResponseEntity.ok(
                medicoService.listarUnoPorPagina(idMedico)
        );
    }

    //@PreAuthorize("hasAuthority('medicos_agregar')")
    @PostMapping("")
    public ResponseEntity<MedicoDTOCompleto> agregar(@Validated @RequestBody MedicoDTOCompleto dto){
        return ResponseEntity.ok(
                medicoService.agregar(dto)
        );
    }

    //@PreAuthorize("hasAuthority('medicos_borrar')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity borrar(@PathVariable(name = "idMedico") int idMedico){
        medicoService.borrar(idMedico);
        return ResponseEntity.ok().build();
    }

    //@PreAuthorize("hasAuthority('medicos_editar')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDTOCompleto> editar(@PathVariable(name = "idMedico") int id, @Validated @RequestBody MedicoDTOCompleto dto){
        return ResponseEntity.ok(
                medicoService.editar(id, dto)
        );
    }
}
