package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.EmptyException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.DTOs.MedicoDTOCompleto;
import com.folcademy.clinica.Model.DTOs.MedicoDTOMostrar;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService implements IMedicoService {

    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    @Override
    public List<MedicoDTOMostrar> listarTodos(){
        List<MedicoDTOMostrar> listaMedicos = ((List<Medico>)medicoRepository.findAll())
                .stream()
                .map(medicoMapper::entityToDTOMostrar)
                .collect(Collectors.toList());
        if(listaMedicos.isEmpty()) throw new EmptyException("No hay medicos en el sistema.");
        return listaMedicos;
    }

    @Override
    public Page<MedicoDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDTOMostrar);
    }

    @Override
    public MedicoDTOMostrar listarUno(Integer id){
        MedicoDTOMostrar medicoMostrar = medicoRepository.findById(id).map(medicoMapper::entityToDTOMostrar).orElse(null);
        if(medicoMostrar == null) throw new NotFoundException("No se encontró el medico especificado");
        return medicoMostrar;
    }

    @Override
    public Page<MedicoDTOMostrar> listarUnoPorPagina(Integer id){
        Pageable pageable = PageRequest.of(0,1);
        Page<Medico> page = medicoRepository.findById(id, pageable);
        if(page.isEmpty()) throw new NotFoundException("No se encontró el medico especificado");
        return page.map(medicoMapper::entityToDTOMostrar);
    }

    @Override
    public MedicoDTOCompleto agregar(MedicoDTOCompleto dto){
        //Error de validacion en el Controller
        if(!personaRepository.existsById(dto.getIdpersona())) throw new NotFoundException("No se encontró la persona a la que se le quiere asignar profesion de medico");

        dto.setId(null);

        dto.setPersona(
                personaRepository.findById(
                        dto.getIdpersona()
                )
                        .map(personaMapper::entityToDTO)
                        .orElse(null)
        );

        return medicoMapper.entityToDTO(
                medicoRepository.save(
                        medicoMapper.DTOToEntity(dto)
                )
        );
    }

    @Override
    public void borrar(Integer id){
        if(!medicoRepository.existsById(id)) throw new NotFoundException("No se encontró el medico especificado");
        medicoRepository.deleteById(id);
    }

    @Override
    public MedicoDTOCompleto editar(Integer idMedico, MedicoDTOCompleto dto){
        //Error de validacion en Controller
        if(!medicoRepository.existsById(idMedico))
            throw new NotFoundException("No existe el medico especificado");
        if(!personaRepository.existsById(dto.getIdpersona()))
            throw new NotFoundException("No se encontró la persona a la que se le quiere asignar profesion de medico");

        dto.setId(idMedico);

        dto.setPersona(
                personaRepository.findById(
                                dto.getIdpersona()
                        )
                        .map(personaMapper::entityToDTO)
                        .orElse(null)
        );

        return
                medicoMapper.entityToDTO(
                    medicoRepository.save(
                          medicoMapper.DTOToEntity(
                                  dto
                          )
                    )
                );
    }

}
