package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.EmptyException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.DTOs.*;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {
    private final TurnoMapper turnoMapper;
    private final TurnoRepository turnoRepository;

    private final PacienteRepository pacienteRepository;

    private final MedicoRepository medicoRepository;

    public TurnoService(TurnoMapper turnoMapper, TurnoRepository turnoRepository, PacienteRepository pacienteRepository, MedicoRepository medicoRepository) {
        this.turnoMapper = turnoMapper;
        this.turnoRepository = turnoRepository;
        this.pacienteRepository = pacienteRepository;
        this.medicoRepository = medicoRepository;
    }

    @Override
    public List<TurnoDTOMostrar> listarTodos() {
        List<TurnoDTOMostrar> turnos = ((List<Turno>)turnoRepository.findAll())
                .stream()
                .map(turnoMapper::entityToDTOMostrar)
                .collect(Collectors.toList());
        if(turnos.isEmpty()) throw new EmptyException("No hay turnos para mostrar");
        return turnos;
    }

    @Override
    public Page<TurnoDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDTOMostrar);
    }

    @Override
    public TurnoDTOMostrar listarUno(Integer id) {
        TurnoDTOMostrar turnoDTOMostrar = turnoRepository.findById(id).map(turnoMapper::entityToDTOMostrar).orElse(null);
        if(turnoDTOMostrar == null) throw new EmptyException("No se encontró el turno especificado");
        return turnoDTOMostrar;
    }

    @Override
    public Page<TurnoDTOMostrar> listarUnoPorPagina(Integer id){
        Pageable pageable = PageRequest.of(0,1);
        Page<Turno> page = turnoRepository.findById(id, pageable);
        if(page.isEmpty()) throw new NotFoundException("No se encontró el turno especificado");
        return page.map(turnoMapper::entityToDTOMostrar);
    }

    @Override
    public TurnoDTOEditarAgregar agregar(TurnoDTOEditarAgregar turno) {
        // Error de validación en el controller
        turno.setIdturno(null);

        //Codigo utilizado tanto en agregar como en editar, se puede reutilizar.
        return editarAgregar(turno);
    }

    @Override
    public void borrar(Integer id) {
       if(!turnoRepository.existsById(id)) throw new NotFoundException("No se encontró el turno especificado");
        turnoRepository.deleteById(id);
    }

    @Override
    public TurnoDTOEditarAgregar editar(Integer id, TurnoDTOEditarAgregar turno) {
        if(!turnoRepository.existsById(id)) throw new NotFoundException("No se encontró el turno especificado");

        turno.setIdturno(id);

        //Codigo utilizado tanto en agregar como en editar, se puede reutilizar.
        return editarAgregar(turno);
    }

    private TurnoDTOEditarAgregar editarAgregar(TurnoDTOEditarAgregar turno){
        // Comprobamos que el idpaciente que se ha enviado sea valido.
        if(!pacienteRepository.existsById(turno.getIdpaciente())) throw new NotFoundException("No se encontró el paciente asignado al turno");

        // De igual manera, comprobamos el idmedico
        if(!medicoRepository.existsById(turno.getIdmedico())) throw new NotFoundException("No se encontró el medico  asignado al turno");

        return turnoMapper.EntityToDTOEditarAgregar(
                turnoRepository.save(
                        turnoMapper.DTOEditarAgregarToEntity(turno)
                )
        );
    }
}
