package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.EmptyException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.DTOs.PacienteDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PacienteDTOMostrar;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;


    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }


    @Override
    public List<PacienteDTOMostrar> findAllPacientes() {
        List<PacienteDTOMostrar> listPacientes = ((List<Paciente>)pacienteRepository.findAll())
                .stream()
                .map(pacienteMapper::entityToDTOMostrar)
                .collect(Collectors.toList());
        if(listPacientes.isEmpty()) throw new EmptyException("No hay pacientes para mostrar");
        return listPacientes;
    }

    @Override
    public Page<PacienteDTOMostrar> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDTOMostrar);
    }

    @Override
    public PacienteDTOMostrar findPacienteById(Integer id) {
        PacienteDTOMostrar paciente = pacienteRepository.findById(id).map(pacienteMapper::entityToDTOMostrar).orElse(null);
        if(paciente == null) throw new NotFoundException("No se encontró el paciente especificado");
        return paciente;
    }

    @Override
    public Page<PacienteDTOMostrar> findPacienteByIdByPage(Integer id){
        Pageable pageable = PageRequest.of(0,1);
        Page<Paciente> page = pacienteRepository.findById(id, pageable);
        if(page.isEmpty()) throw new NotFoundException("No se encontró el medico especificado");
        return page.map(pacienteMapper::entityToDTOMostrar);
    }

    @Override
    public PacienteDTOCompleto savePaciente(PacienteDTOCompleto dto){
        //Error de validacion en el Controller
        if(!personaRepository.existsById(dto.getIdpersona())) throw new NotFoundException("No se encontró la persona a la que se le quiere asignar como paciente");

        dto.setId(null);

        dto.setPersona(
                personaRepository.findById(dto.getIdpersona())
                        .map(personaMapper::entityToDTO)
                        .orElse(null)
        );

        return pacienteMapper.entityToDTO(
                pacienteRepository.save(
                        pacienteMapper.DTOToEntity(dto)
                )
        );
    }

    @Override
    public void deleteById(Integer id){
        if(!pacienteRepository.existsById(id))  throw new NotFoundException("No se encontró el paciente especificado");
        pacienteRepository.deleteById(id);
    }

    @Override
    public PacienteDTOCompleto edit(Integer id, PacienteDTOCompleto dto){
        //Error de validacion en Controller
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No se encontró el paciente especificado");

        if(!personaRepository.existsById(dto.getIdpersona()))
            throw new NotFoundException("No se encontró la persona a la que se le quiere asignar profesion de medico");

        dto.setId(id);

        dto.setPersona(
                personaRepository.findById(
                                dto.getIdpersona()
                        )
                        .map(personaMapper::entityToDTO)
                        .orElse(null)
        );

        return
                pacienteMapper.entityToDTO(
                    pacienteRepository.save(
                            pacienteMapper.DTOToEntity(dto)
                    )
                );
    }
}
