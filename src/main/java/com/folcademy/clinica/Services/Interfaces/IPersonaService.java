package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTOs.PersonaDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PersonaDTOMostrar;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPersonaService {
    List<PersonaDTOMostrar> listarTodos();
    Page<PersonaDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField);
    PersonaDTOMostrar listarUno(Integer id);
    Page<PersonaDTOMostrar> listarUnoPorPagina(Integer id);
    PersonaDTOCompleto agregar(PersonaDTOCompleto dto);
    void borrar(Integer id);
    PersonaDTOCompleto editar(Integer idPersona, PersonaDTOCompleto dto);
}
