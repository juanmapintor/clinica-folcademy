package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTOs.TurnoDTOEditarAgregar;
import com.folcademy.clinica.Model.DTOs.TurnoDTOMostrar;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ITurnoService {
    List<TurnoDTOMostrar> listarTodos();
    Page<TurnoDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField);
    TurnoDTOMostrar listarUno(Integer id);
    Page<TurnoDTOMostrar> listarUnoPorPagina(Integer id);
    TurnoDTOEditarAgregar agregar(TurnoDTOEditarAgregar turno);
    void borrar(Integer id);
    TurnoDTOEditarAgregar editar(Integer idTurno, TurnoDTOEditarAgregar turno);
}
