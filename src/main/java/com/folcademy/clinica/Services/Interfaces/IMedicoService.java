package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTOs.MedicoDTOCompleto;
import com.folcademy.clinica.Model.DTOs.MedicoDTOMostrar;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IMedicoService {
    List<MedicoDTOMostrar> listarTodos();
    Page<MedicoDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField);
    MedicoDTOMostrar listarUno(Integer id);
    Page<MedicoDTOMostrar> listarUnoPorPagina(Integer id);
    MedicoDTOCompleto agregar(MedicoDTOCompleto dto);
    void borrar(Integer id);
    MedicoDTOCompleto editar(Integer idMedico, MedicoDTOCompleto dto);
}
