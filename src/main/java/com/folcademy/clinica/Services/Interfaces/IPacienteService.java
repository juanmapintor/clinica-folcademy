package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTOs.PacienteDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PacienteDTOMostrar;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPacienteService {
    List<PacienteDTOMostrar> findAllPacientes();
    Page<PacienteDTOMostrar> findAllByPage(Integer pageNumber, Integer pageSize, String orderField);
    PacienteDTOMostrar findPacienteById(Integer id);
    Page<PacienteDTOMostrar> findPacienteByIdByPage(Integer id);
    PacienteDTOCompleto savePaciente(PacienteDTOCompleto dto);
    void deleteById(Integer id);
    PacienteDTOCompleto edit(Integer id, PacienteDTOCompleto dto);

}
