package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.EmptyException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.DTOs.PersonaDTOCompleto;
import com.folcademy.clinica.Model.DTOs.PersonaDTOMostrar;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPersonaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("personaService")
public class PersonaService implements IPersonaService {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PersonaService(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    @Override
    public List<PersonaDTOMostrar> listarTodos(){
        List<PersonaDTOMostrar> listaPersonas = ((List<Persona>)personaRepository.findAll())
                .stream()
                .map(personaMapper::entityToDTOMostrar)
                .collect(Collectors.toList());
        if(listaPersonas.isEmpty()) throw new EmptyException("No hay personas en el sistema.");
        return listaPersonas;
    }

    @Override
    public Page<PersonaDTOMostrar> listarTodosPorPagina(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return personaRepository.findAll(pageable).map(personaMapper::entityToDTOMostrar);
    }

    @Override
    public PersonaDTOMostrar listarUno(Integer id){
        PersonaDTOMostrar personaMostrar = personaRepository.findById(id).map(personaMapper::entityToDTOMostrar).orElse(null);
        if(personaMostrar == null) throw new NotFoundException("No se encontró la persona especificada");
        return personaMostrar;
    }

    @Override
    public Page<PersonaDTOMostrar> listarUnoPorPagina(Integer id){
        Pageable pageable = PageRequest.of(0,1);
        Page<Persona> page = personaRepository.findById(id, pageable);
        if(page.isEmpty()) throw new NotFoundException("No se encontró la persona especificado");
        return page.map(personaMapper::entityToDTOMostrar);
    }

    @Override
    public PersonaDTOCompleto agregar(PersonaDTOCompleto dto){
        //Error de validacion en el Controller
        return personaMapper.entityToDTO(
                personaRepository.save(
                        personaMapper.DTOToEntity(
                                dto
                        )
                )
        );
    }

    @Override
    public void borrar(Integer id){
        if(!personaRepository.existsById(id)) throw new NotFoundException("No se encontró la persona especificado");
        personaRepository.deleteById(id);
    }

    @Override
    public PersonaDTOCompleto editar(Integer idPersona, PersonaDTOCompleto dto){
        //Error de validacion en Controller
        if(!personaRepository.existsById(idPersona))
            throw new NotFoundException("No existe la persona especificado");
        dto.setId(idPersona);
        return
                personaMapper.entityToDTO(
                        personaRepository.save(
                                personaMapper.DTOToEntity(
                                        dto
                                )
                        )
                );
    }
}
