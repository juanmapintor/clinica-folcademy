-- MySQL dump 10.13  Distrib 8.0.27, for macos11 (x86_64)
--
-- Host: localhost    Database: clinica-folcademy-auth
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (6,'pacientes_findAll',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(7,'pacientes_findById',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(8,'pacientes_save',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(9,'pacientes_edit',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(10,'pacientes_deleteById',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(11,'medicos_listarTodos',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(12,'medicos_listarUno',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(13,'medicos_agregar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(14,'medicos_modificar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(15,'medicos_borrar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(16,'turnos_listarTodos',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(17,'turnos_listarUno',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(18,'turnos_agregar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(19,'turnos_modificar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(20,'turnos_borrar',0,'2021-11-28 16:52:31','2021-11-28 16:52:31'),(21,'medicos_listarTodosPorPagina',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(22,'pacientes_findAllByPage',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(23,'turnos_listarTodosPorPagina',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(24,'personas_listarTodos',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(25,'personas_listarTodosPorPagina',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(26,'personas_listarUno',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(27,'personas_agregar',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(28,'personas_borrar',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(29,'personas_editar',0,'2021-12-11 03:20:28','2021-12-11 03:20:28'),(30,'medicos_listarUnoPorPagina',0,'2021-12-11 18:10:22','2021-12-11 18:10:22'),(31,'pacientes_findByIdByPage',0,'2021-12-11 18:10:22','2021-12-11 18:10:22'),(32,'personas_listarUnoPorPagina',0,'2021-12-11 18:10:22','2021-12-11 18:10:22'),(33,'turnos_listarUnoPorPagina',0,'2021-12-11 18:10:22','2021-12-11 18:10:22');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorized_grant_types`
--

DROP TABLE IF EXISTS `authorized_grant_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorized_grant_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorized_grant_types`
--

LOCK TABLES `authorized_grant_types` WRITE;
/*!40000 ALTER TABLE `authorized_grant_types` DISABLE KEYS */;
INSERT INTO `authorized_grant_types` VALUES (1,'password',0,'2020-10-01 13:35:24','2020-10-01 13:35:24'),(2,'client_credentials',0,'2020-10-01 13:35:24','2020-10-01 13:35:24'),(3,'refresh_token',0,'2020-10-01 13:35:25','2020-10-01 13:35:25');
/*!40000 ALTER TABLE `authorized_grant_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medico` (
  `idmedico` int NOT NULL AUTO_INCREMENT,
  `profesion` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `consulta` int DEFAULT NULL,
  `idpersona` int NOT NULL,
  PRIMARY KEY (`idmedico`),
  KEY `medico_persona_idpersona_fk` (`idpersona`),
  CONSTRAINT `medico_persona_idpersona_fk` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES (18,'ENDOCRINOLOGIA/NUTICION',4581,42),(19,'FLEBOLOGIA',5427,43),(20,'ALERGIA INFANTIL',3284,44),(21,'NEFROLOGIA',5672,45),(22,'NEUROLOGIA INFANTIL',3462,46),(23,'OFTALMOLOGIA',3208,47),(24,'HEMATOLOGIA INFANTIL',1673,48),(25,'ECOGRAFIA',3901,49),(26,'HEMOTERAPIA',4294,50),(27,'ONCOLOGIA',2102,51),(28,'CIRUGIA PROCTOLOGICA',4109,52),(29,'NEUROCIRUGIA',5316,53),(30,'CIRUGIA ONCOLOGICA',3105,54),(31,'NEUMOTISIOLOGIA INFANTIL',3344,55),(32,'MEDICINA NUCLEAR',4365,56),(33,'CLINICA ESTOMATOLOGICA',4774,57);
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `token` mediumblob,
  `authentication_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `authentication` mediumblob,
  `refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_token`
--

LOCK TABLES `oauth_access_token` WRITE;
/*!40000 ALTER TABLE `oauth_access_token` DISABLE KEYS */;
INSERT INTO `oauth_access_token` VALUES ('ad9336d56f39e35041cf0cfbecc5608a',_binary '�\�\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$�\�\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.Collections$EmptyMapY6�Z\�\�\�\0\0xpsr\0java.util.Datehj�KYt\0\0xpw\0\0}���xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/\�Gc�\�ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens\�\ncT\�^\0L\0valueq\0~\0xpt\0$80229af9-f80e-4fe7-bdcf-741e2ac3629dsq\0~\0	w\0\0~H2\�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0Readt\0Writext\0bearert\0$9928c85c-0b65-480a-85db-73f4928669c0','946554a8bb3ee810d17aa2b3e5747c04','user','user',_binary '�\�\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0&\0L\0rolet\0Ljava/lang/String;xpt\0medicos_listarTodossq\0~\0\rt\0medicos_listarTodosPorPaginasq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0pacientes_findAllByPagesq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0personas_listarTodossq\0~\0\rt\0personas_listarTodosPorPaginasq\0~\0\rt\0personas_listarUnosq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0turnos_listarTodosPorPaginasq\0~\0\rt\0turnos_listarUnoxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0*xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0(L\0scopeq\0~\0*xpt\0usersr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0(xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0usernamet\0userxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0Readt\0Writexsq\0~\09w\0\0\0 ?@\0\0\0\0\0sq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0medicos_listarTodossq\0~\0\rt\0turnos_listarTodosPorPaginasq\0~\0\rt\0personas_listarUnosq\0~\0\rt\0personas_listarTodosPorPaginasq\0~\0\rt\0medicos_listarTodosPorPaginasq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0turnos_listarUnosq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0pacientes_findAllByPagesq\0~\0\rt\0personas_listarTodosxsq\0~\00?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\09w\0\0\0?@\0\0\0\0\0t\0API_CLINICAxsq\0~\09w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0&\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%xq\0~\0]sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\00?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\02q\0~\03q\0~\04q\0~\05x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0&\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0*L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\06sr\0java.util.TreeSetݘP��\�[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0&\0\0xpw\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%xpt\0user','0b474bef27b247a1aeba0035f9704479');
/*!40000 ALTER TABLE `oauth_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `token` mediumblob,
  `authentication` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_token`
--

LOCK TABLES `oauth_refresh_token` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_token` DISABLE KEYS */;
INSERT INTO `oauth_refresh_token` VALUES ('29d34ce8a89a8911d51fa23684fa0a35',_binary '�\�\0sr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/\�Gc�\�ɷ\0L\0\nexpirationt\0Ljava/util/Date;xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens\�\ncT\�^\0L\0valuet\0Ljava/lang/String;xpt\0$18b76d0d-1910-4d1c-b558-02024239d916sr\0java.util.Datehj�KYt\0\0xpw\0\0~Јx',_binary '�\�\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0&\0L\0rolet\0Ljava/lang/String;xpt\0deletesq\0~\0\rt\0getsq\0~\0\rt\0postsq\0~\0\rt\0putxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0\Zxr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0L\0scopeq\0~\0\Zxpt\0usersr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0usernamet\0adminxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0Readt\0Writexsq\0~\0)w\0\0\0?@\0\0\0\0\0sq\0~\0\rt\0getxsq\0~\0 ?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\0)w\0\0\0?@\0\0\0\0\0t\0API_CLINICAxsq\0~\0)w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0&\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0xq\0~\07sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\0 ?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\0\"q\0~\0#q\0~\0$q\0~\0%x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0&\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0\ZL\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\0&sr\0java.util.TreeSetݘP��\�[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0&\0\0xpw\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0xpt\0admin'),('d8fe2027e47b011571fd01a45276151f',_binary '�\�\0sr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/\�Gc�\�ɷ\0L\0\nexpirationt\0Ljava/util/Date;xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens\�\ncT\�^\0L\0valuet\0Ljava/lang/String;xpt\0$1ba2c2b2-0922-4bca-abcd-7534db326d13sr\0java.util.Datehj�KYt\0\0xpw\0\0~г�x',_binary '�\�\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0&\0L\0rolet\0Ljava/lang/String;xpt\0medicos_agregarsq\0~\0\rt\0medicos_borrarsq\0~\0\rt\0medicos_listarTodossq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0medicos_modificarsq\0~\0\rt\0pacientes_deleteByIdsq\0~\0\rt\0pacientes_editsq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0pacientes_savesq\0~\0\rt\0turnos_agregarsq\0~\0\rt\0\rturnos_borrarsq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0turnos_listarUnosq\0~\0\rt\0turnos_modificarxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\00xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0.L\0scopeq\0~\00xpt\0adminsr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0.xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0usernamet\0adminxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0Readt\0Writexsq\0~\0?w\0\0\0 ?@\0\0\0\0\0sq\0~\0\rt\0pacientes_savesq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0medicos_listarTodossq\0~\0\rt\0turnos_modificarsq\0~\0\rt\0medicos_agregarsq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0turnos_listarUnosq\0~\0\rt\0pacientes_editsq\0~\0\rt\0pacientes_deleteByIdsq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0medicos_modificarsq\0~\0\rt\0\rturnos_borrarsq\0~\0\rt\0medicos_borrarsq\0~\0\rt\0turnos_agregarxsq\0~\06?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\0?w\0\0\0?@\0\0\0\0\0t\0API_CLINICAxsq\0~\0?w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0&\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%q\0~\0\'q\0~\0)q\0~\0+xq\0~\0isr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\06?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\08q\0~\09q\0~\0:q\0~\0;x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0&\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\00L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\0<sr\0java.util.TreeSetݘP��\�[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0&\0\0xpw\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%q\0~\0\'q\0~\0)q\0~\0+xpt\0admin'),('0b474bef27b247a1aeba0035f9704479',_binary '�\�\0sr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/\�Gc�\�ɷ\0L\0\nexpirationt\0Ljava/util/Date;xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens\�\ncT\�^\0L\0valuet\0Ljava/lang/String;xpt\0$80229af9-f80e-4fe7-bdcf-741e2ac3629dsr\0java.util.Datehj�KYt\0\0xpw\0\0~H2\�x',_binary '�\�\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0&\0L\0rolet\0Ljava/lang/String;xpt\0medicos_listarTodossq\0~\0\rt\0medicos_listarTodosPorPaginasq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0pacientes_findAllByPagesq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0personas_listarTodossq\0~\0\rt\0personas_listarTodosPorPaginasq\0~\0\rt\0personas_listarUnosq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0turnos_listarTodosPorPaginasq\0~\0\rt\0turnos_listarUnoxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0*xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0(L\0scopeq\0~\0*xpt\0usersr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0(xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0usernamet\0userxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0Readt\0Writexsq\0~\09w\0\0\0 ?@\0\0\0\0\0sq\0~\0\rt\0turnos_listarTodossq\0~\0\rt\0medicos_listarTodossq\0~\0\rt\0turnos_listarTodosPorPaginasq\0~\0\rt\0personas_listarUnosq\0~\0\rt\0personas_listarTodosPorPaginasq\0~\0\rt\0medicos_listarTodosPorPaginasq\0~\0\rt\0pacientes_findAllsq\0~\0\rt\0turnos_listarUnosq\0~\0\rt\0medicos_listarUnosq\0~\0\rt\0pacientes_findByIdsq\0~\0\rt\0pacientes_findAllByPagesq\0~\0\rt\0personas_listarTodosxsq\0~\00?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\09w\0\0\0?@\0\0\0\0\0t\0API_CLINICAxsq\0~\09w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0&\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%xq\0~\0]sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\00?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\02q\0~\03q\0~\04q\0~\05x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0&\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0*L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\06sr\0java.util.TreeSetݘP��\�[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0&\0\0xpw\0\0\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0q\0~\0!q\0~\0#q\0~\0%xpt\0user');
/*!40000 ALTER TABLE `oauth_refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paciente` (
  `idpaciente` int NOT NULL AUTO_INCREMENT,
  `direccion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idpersona` int NOT NULL,
  PRIMARY KEY (`idpaciente`),
  UNIQUE KEY `paciente_idpersona_uindex` (`idpersona`),
  CONSTRAINT `paciente_persona_fk` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (8,'02 Heffernan Pass',28),(9,'7 Veith Terrace',29),(10,'73013 Green Way',30),(11,'613 Sheridan Plaza',31),(12,'6350 Kensington Street',32),(13,'32474 Little Fleur Road',33),(14,'9 Doe Crossing Center',34),(15,'04297 Clemons Court',35),(16,'14869 Evergreen Street',36),(17,'482 Rockefeller Way',37),(18,'60 Old Gate Plaza',38),(19,'713 Grasskamp Drive',39),(20,'822 Tomscot Park',40),(21,'9 Graedel Point',41),(22,'530 Leroy Terrace',42),(23,'39184 8th Plaza',43),(24,'72568 Cherokee Road',44),(25,'4330 Southport Street',45),(26,'2874 Monument Circle',46),(27,'5 Sycamore Lane',47),(28,'47626 Badeau Lane',48),(29,'851 Coleman Pass',49);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `idpersona` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `dni` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpersona`),
  UNIQUE KEY `persona_dni_uindex` (`dni`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (28,'Florence','Turbefield','9905366695','961 355 6539'),(29,'Daveen','Toye','6435199385','841 481 3095'),(30,'Ethel','McIlheran','6694336559','704 771 7527'),(31,'Madelle','Malcolm','1482951723','132 391 5405'),(32,'Barth','Cotesford','4116353132','515 853 6650'),(33,'Kirby','Fernao','1773652542','995 646 0351'),(34,'Josephina','Jerrams','1043879096','756 509 1606'),(35,'Anderea','Steggles','9334767666','101 507 6267'),(36,'Wilton','Heddan','3143498114','195 442 8993'),(37,'Camala','Tirkin','7558600400','734 889 6278'),(38,'Mark','Cruce','9234232467','839 239 9654'),(39,'Meagan','Fernie','9964305749','948 291 7921'),(40,'Goldie','Connal','8187614050','650 240 1934'),(41,'Zollie','Edel','5532858893','483 239 3045'),(42,'Robin','Drysdale','9177461100','204 863 8189'),(43,'Laurent','Hawley','4041877472','279 225 6903'),(44,'Tan','Scaplehorn','1148635950','786 715 0509'),(45,'Meade','Teece','9406535648','429 508 5473'),(46,'Briant','Tancock','4857501129','993 685 9222'),(47,'Ezri','Embling','9972518888','417 352 6122'),(48,'Camellia','Kempstone','4052102796','705 240 3372'),(49,'Reynard','Chalke','8003180646','964 931 0106'),(50,'Denny','Semper','7653506431','324 747 4983'),(51,'Roscoe','Whereat','6489919948','144 535 2854'),(52,'Sarette','Rubinsohn','8055425932','608 637 6466'),(53,'Yevette','Arlt','4115439500','965 531 3309'),(54,'Warden','Playfoot','8718668322','125 348 2383'),(55,'Hirsch','MacAlester','1921910210','630 157 2113'),(56,'Jaimie','Caslett','8795619807','423 396 1159'),(57,'Valeda','Feather','2717149790','199 393 6263'),(58,'Joe Joe','Doe Doe','22000000','4222222');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resources` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,'API_CLINICA',0,'2021-11-16 17:11:15','2021-11-16 17:11:15');
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scopes`
--

DROP TABLE IF EXISTS `scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scopes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scopes`
--

LOCK TABLES `scopes` WRITE;
/*!40000 ALTER TABLE `scopes` DISABLE KEYS */;
INSERT INTO `scopes` VALUES (1,'Read',0,'2021-11-16 17:10:30','2021-11-16 17:10:30'),(2,'Write',0,'2021-11-16 17:10:30','2021-11-16 17:10:30');
/*!40000 ALTER TABLE `scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turno` (
  `idturno` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `atendido` tinyint DEFAULT '0',
  `idpaciente` int DEFAULT NULL,
  `idmedico` int DEFAULT NULL,
  PRIMARY KEY (`idturno`),
  KEY `fk_medico_idx` (`idmedico`),
  KEY `fk_paciente_idx` (`idpaciente`),
  CONSTRAINT `fk_medico` FOREIGN KEY (`idmedico`) REFERENCES `medico` (`idmedico`),
  CONSTRAINT `fk_paciente` FOREIGN KEY (`idpaciente`) REFERENCES `paciente` (`idpaciente`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
INSERT INTO `turno` VALUES (3,'2021-10-26','16:02:00',0,8,32),(4,'2020-12-25','11:24:00',1,19,20),(5,'2021-05-23','14:16:00',0,21,28),(6,'2021-06-05','09:37:00',0,28,21),(7,'2020-12-26','11:28:00',1,16,22),(8,'2021-04-20','08:36:00',0,12,23),(9,'2021-09-29','10:54:00',0,29,25),(10,'2021-05-21','06:00:00',1,21,25),(12,'2020-12-26','10:09:00',1,13,26),(13,'2021-11-22','09:15:00',0,25,33),(14,'2021-11-09','14:41:00',1,11,28),(15,'2021-05-12','14:25:00',0,11,32),(16,'2021-11-05','13:12:00',1,28,22),(17,'2021-09-01','11:43:00',0,22,22),(18,'2021-07-10','09:45:00',1,11,19),(19,'2021-04-29','15:58:00',1,8,18),(20,'2021-05-09','14:47:00',1,10,19),(21,'2021-11-07','09:01:00',0,20,32),(22,'2021-11-21','16:55:00',1,12,28),(23,'2222-12-22','22:22:22',1,16,26);
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `usuarioId` int unsigned NOT NULL AUTO_INCREMENT,
  `usuarioCodigo` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuarioClave` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuarioDesc` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT '',
  `usuarioHabi` double(1,0) NOT NULL DEFAULT '0',
  `usuarioDelete` int NOT NULL DEFAULT '0',
  `accesstokenvalidityseconds` int NOT NULL DEFAULT '3600',
  `refreshtokenvalidityseconds` int NOT NULL DEFAULT '2629800',
  PRIMARY KEY (`usuarioId`),
  KEY `idx_acc_codigo` (`usuarioCodigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'0001','$2a$10$yC.xsA/G2.RUXfdf2aT6wemYl42iuzl0T/AUANUC7VCozOVwqAMou','admin',1,0,3600,2629800),(2,'0002','$2a$10$Z1SEqsJi4JRYAtD.Clbw.e8r7dleYuFJ3j5Mu3/KI8cSRtgMmbx.u','user',1,0,3600,2629800);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_authorities`
--

DROP TABLE IF EXISTS `usuarios_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_authorities` (
  `usuarioId` int unsigned NOT NULL,
  `id_authority` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_authority`),
  KEY `authority_fk` (`id_authority`),
  CONSTRAINT `authority_fk` FOREIGN KEY (`id_authority`) REFERENCES `authorities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_authorities`
--

LOCK TABLES `usuarios_authorities` WRITE;
/*!40000 ALTER TABLE `usuarios_authorities` DISABLE KEYS */;
INSERT INTO `usuarios_authorities` VALUES (1,6),(2,6),(1,7),(2,7),(1,8),(1,9),(1,10),(1,11),(2,11),(1,12),(2,12),(1,13),(1,14),(1,15),(1,16),(2,16),(1,17),(2,17),(1,18),(1,19),(1,20),(1,21),(2,21),(1,22),(2,22),(1,23),(2,23),(1,24),(2,24),(1,25),(2,25),(1,26),(2,26),(1,27),(1,28),(1,29),(1,30),(2,30),(1,31),(2,31),(1,32),(2,32),(1,33),(2,33);
/*!40000 ALTER TABLE `usuarios_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_authorized_grant_types`
--

DROP TABLE IF EXISTS `usuarios_authorized_grant_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_authorized_grant_types` (
  `usuarioId` int unsigned NOT NULL,
  `id_authorized_grant_types` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_authorized_grant_types`),
  KEY `authorized_grant_types_fk` (`id_authorized_grant_types`),
  CONSTRAINT `authorized_grant_types_fk` FOREIGN KEY (`id_authorized_grant_types`) REFERENCES `authorized_grant_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk4` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_authorized_grant_types`
--

LOCK TABLES `usuarios_authorized_grant_types` WRITE;
/*!40000 ALTER TABLE `usuarios_authorized_grant_types` DISABLE KEYS */;
INSERT INTO `usuarios_authorized_grant_types` VALUES (1,1),(2,1),(1,2),(2,2),(1,3),(2,3);
/*!40000 ALTER TABLE `usuarios_authorized_grant_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_resources`
--

DROP TABLE IF EXISTS `usuarios_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_resources` (
  `usuarioId` int unsigned NOT NULL,
  `id_resource` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_resource`),
  KEY `resource_fk` (`id_resource`),
  CONSTRAINT `resource_fk` FOREIGN KEY (`id_resource`) REFERENCES `resources` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk3` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_resources`
--

LOCK TABLES `usuarios_resources` WRITE;
/*!40000 ALTER TABLE `usuarios_resources` DISABLE KEYS */;
INSERT INTO `usuarios_resources` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `usuarios_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_scopes`
--

DROP TABLE IF EXISTS `usuarios_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_scopes` (
  `usuarioId` int unsigned NOT NULL,
  `id_scope` int NOT NULL,
  PRIMARY KEY (`usuarioId`,`id_scope`),
  KEY `scope_fk` (`id_scope`),
  CONSTRAINT `scope_fk` FOREIGN KEY (`id_scope`) REFERENCES `scopes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_fk2` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`usuarioId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_scopes`
--

LOCK TABLES `usuarios_scopes` WRITE;
/*!40000 ALTER TABLE `usuarios_scopes` DISABLE KEYS */;
INSERT INTO `usuarios_scopes` VALUES (1,1),(2,1),(1,2),(2,2);
/*!40000 ALTER TABLE `usuarios_scopes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-11 18:21:55
